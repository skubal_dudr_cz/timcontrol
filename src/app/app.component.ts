import { Component, OnInit } from '@angular/core';
import { Observable, interval } from 'rxjs';
import { map, takeWhile, finalize } from 'rxjs/operators';

import { environment } from '../environments/environment';
import { TimcontrolVersionCheckService } from 'timcontrol-version-check';

import packageVersionCheck from '../../projects/timcontrol-version-check/package.json';
import packageCodeReader from '../../projects/timcontrol-code-reader/package.json';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  title = 'TimControl libraries';
  libraryVersions = {};

  // version check
  private versionCheckFrequency = 1000 * 60 * 10; // every 10 minutes
  private versionCountdownCounter: number;
  versionCountdown$: Observable<number>;

  // code reader
  code: string;
  type: string;
  isLoaded = false;

  constructor(private versionCheckService: TimcontrolVersionCheckService) {}

  ngOnInit(): void {
    if (environment.production) {
      this.versionCheckService.initVersionCheck('/version.json', this.versionCheckFrequency, this.newVersionDetected);
      this.versionCountdownInit();
    }

    this.libraryVersions[packageVersionCheck.name] = packageVersionCheck.version;
    this.libraryVersions[packageCodeReader.name] = packageCodeReader.version;
  }

  private newVersionDetected(hash: string): void {
    console.log('New version has been detected, reloading…', hash);
    location.reload();
  }

  private versionCountdownInit() {
    const timer = interval(1000);
    this.versionCountdownCounter = this.versionCheckFrequency - 1000;
    this.versionCountdown$ = timer.pipe(
      map(i => this.versionCountdownCounter - i * 1000),
      takeWhile(i => i > 0),
      finalize(() => this.versionCountdownInit())
    );
  }

  receiveRfidCode($event) {
    this.code = $event;
    this.type = 'RFID';
    this.isLoaded = true;
  }
  receiveQrCode($event) {
    this.code = $event;
    this.type = 'QR';
    this.isLoaded = true;
  }

}
