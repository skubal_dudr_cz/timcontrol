import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { TimcontrolVersionCheckService } from 'timcontrol-version-check';
import { TimcontrolCodeReaderModule } from 'timcontrol-code-reader';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    TimcontrolCodeReaderModule
  ],
  providers: [HttpClientModule, TimcontrolVersionCheckService],
  bootstrap: [AppComponent]
})
export class AppModule { }
