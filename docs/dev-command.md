# Vývojářské příkazy
Zde je seznam příkazů, které byly použity při generování tohoto projektu.

## Vygenerování nového projektu

````shell
ng new timcontrol --defaults=true && cd timcontrol
````

## Vygenerování knihovny `timcontrol-version-check`

````shell
ng generate library timcontrol-version-check --prefix=timcontrol-version-check
````

## Build knihovny `timcontrol-version-check`

````shell
cd projects/timcontrol-version-check && ng build timcontrol-version-check && cd ../..
````

## Vytvoření knihovny `timcontrol-version-check`

````shell
npm run package timcontrol-version-check
````

## Vygenerování knihovny `timcontrol-code-reader`

````shell
ng generate library timcontrol-code-reader --prefix=timcontrol-code-reader
````

## Build knihovny `timcontrol-code-reader`

````shell
cd projects/timcontrol-code-reader && ng build timcontrol-code-reader && cd ../..
````

## Vytvoření knihovny `timcontrol-version-check`

````shell
npm run package timcontrol-code-reader
````

