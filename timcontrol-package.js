/*jshint esversion: 6 */

const execSync = require('child_process').execSync;
const fs = require('fs');

const libraryName = process.argv[2] || ''; // Default value `` if no args provided via CLI.

// build
execSync('ng build ' + libraryName, {stdio:[0, 1, 2]});

// copy lib assets
if (fs.existsSync('projects/' + libraryName + '/src/assets')) {
  execSync('cp -r projects/' + libraryName + '/src/assets/[^.]* "dist/' + libraryName + '/"', {stdio:[0, 1, 2]});
}

// package
execSync('cd "dist/' + libraryName + '" && npm pack', {stdio:[0, 1, 2]});
