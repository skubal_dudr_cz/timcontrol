# Timcontrol knihovny pro projekty v Angular 8+


## Version check

Automatická kontrola verze aplikace pro Angular 8+.

[repozitář](https://bitbucket.org/skubal_dudr_cz/timcontrol/src/master/projects/timcontrol-version-check/ "repozitář BitBucket")

Příkaz pro build a vygenerování balíčku:

````shell
npm run package timcontrol-version-check
````


## Code reader

Čtečka QR/RFID pro Angular 8+.

[repozitář](https://bitbucket.org/skubal_dudr_cz/timcontrol/src/master/projects/timcontrol-code-reader/ "repozitář BitBucket")

Příkaz pro build a vygenerování balíčku:

````shell
npm run package timcontrol-code-reader
````

***

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.1.2.
