/*
 * Public API Surface of timcontrol-code-reader
 */

export * from './lib/timcontrol-code-reader.module';
export * from './lib/timcontrol-code-reader.component';
