import { NgModule } from '@angular/core';
import { TimcontrolCodeReaderComponent } from './timcontrol-code-reader.component';

@NgModule({
  declarations: [TimcontrolCodeReaderComponent],
  imports: [],
  exports: [TimcontrolCodeReaderComponent],
})
export class TimcontrolCodeReaderModule {}
