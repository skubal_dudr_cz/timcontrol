import { Component, OnInit, OnDestroy, Output, EventEmitter, HostListener } from '@angular/core';
import { of } from 'rxjs';
import { delay, mapTo } from 'rxjs/operators';

const TIMEOUT = 170; // maximum time for entering the entire code

export interface CodeReaderStatus {
  code: string;
  type: CodeType;
}

export enum CodeType {
  Rfid = 'rfid',
  Qr = 'qr',
}

@Component({
  selector: 'timcontrol-code-reader',
  template: `
    <span id="code_reader"></span>
  `,
  styles: [],
})
export class TimcontrolCodeReaderComponent implements OnInit, OnDestroy {
  public suspendedEvents: KeyboardEvent[];
  private isSuspended = true;
  public isLoaded = false;
  public status: CodeReaderStatus;

  @Output() loadRfidEvent = new EventEmitter<string>();
  @Output() loadQrEvent = new EventEmitter<string>();

  constructor() {}

  ngOnInit() {
    this.suspendedEvents = [];
    this.status = {
      code: null,
      type: null,
    };
  }

  ngOnDestroy() {
    this.suspendedEvents = [];
  }

  @HostListener('window:keydown', ['$event']) onKeydownHandler(event: KeyboardEvent) {
    const emmiter = of(null);
    if (
      this.isSuspended &&
      event.code &&
      event.code.substr(0, 5) === 'Digit' &&
      (!this.suspendedEvents.length || this.suspendedEvents[this.suspendedEvents.length - 1].target === event.target)
    ) {
      if (this.isLoaded) {
        this.status.type = null;
        this.isLoaded = false;
      }
      // TODO: when entering 11 characters, call the event with the first character
      // TODO: watch delay between entering the individual characters instead of monitoring time to enter the entire code
      this.suspendedEvents.push(event);
      const subscribe = emmiter
        .pipe(
          mapTo(this.suspendedEvents.length),
          delay(TIMEOUT)
        )
        .subscribe(length => {
          this.applyEvents(this.suspendedEvents);
        });

      return false;
    }

    if (event.code === 'Minus' && !this.status.type) {
      this.status.type = CodeType.Qr;
      return false;
    }

    if (event.code === 'Enter') {
      if (this.suspendedEvents.length === 10) {
        this.status.code = this.loadCodeFromEvents(this.suspendedEvents);
        this.isLoaded = true;
        if (!this.status.type) {
          this.status.type = CodeType.Rfid;
        }
        switch (this.status.type) {
          case CodeType.Rfid: {
            this.loadRfidEvent.emit(this.status.code);
            break;
          }
          case CodeType.Qr: {
            this.loadQrEvent.emit(this.status.code);
            break;
          }
          default: {
            break;
          }
        }
      }
    }
    if (this.suspendedEvents.length) {
      this.applyEvents(this.suspendedEvents);
    }
    return true;
  }

  private applyEvents(events: KeyboardEvent[]) {
    let event: KeyboardEvent;
    const activeElement = document.activeElement as HTMLElement;
    while (events.length) {
      event = events.shift();
      const el = event.target as HTMLInputElement;
      this.isSuspended = false;
      const ev = new KeyboardEvent('keydown', {
        code: event.code,
        key: event.key,
        bubbles: true,
      });
      if (el.nodeName === 'INPUT' || el.nodeName === 'TEXTAREA') {
        let position = el.selectionStart;
        el.value = el.value.slice(0, position) + event.key + el.value.slice(el.selectionEnd);
        el.focus();
        position++;
        el.setSelectionRange(position, position);
      }
      el.dispatchEvent(ev);
    }
    activeElement.focus();
    this.isSuspended = true;
  }

  private loadCodeFromEvents(events: KeyboardEvent[]) {
    let code = '';
    let event: KeyboardEvent;
    while (events.length) {
      event = events.shift();
      code += event.code.substr(-1);
    }
    return code;
  }
}
