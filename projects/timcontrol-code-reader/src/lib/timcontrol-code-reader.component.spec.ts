import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TimcontrolCodeReaderComponent } from './timcontrol-code-reader.component';

describe('TimcontrolCodeReaderComponent', () => {
  let component: TimcontrolCodeReaderComponent;
  let fixture: ComponentFixture<TimcontrolCodeReaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TimcontrolCodeReaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TimcontrolCodeReaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
