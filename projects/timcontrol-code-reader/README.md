# TimcontrolCodeReader

## Installation

Add library to `package.json` via bash:

```shell
npm install https://bitbucket.org/skubal_dudr_cz/timcontrol/downloads/timcontrol-code-reader-0.0.1.tgz --save
```

Then add code to template. For example:

```html
<timcontrol-code-reader (loadRfidEvent)="receiveRfid($event)" (loadQrEvent)="receiveQr($event)"></timcontrol-code-reader>
```

Function callbacks in `loadRfidEvent` and `loadQrEvent` are fired whenever code of its type is detected.

## Requirements

* Angular 8+

## How it works

The component is capturing keystrokes. If keydown event by selected keys is captured, the component defers its event and stores it in a queue.
If the queue contains a series of keypresses 10 or 11 numkeys followed by enter key (standard behaviour of code readers) in reserved time (170ms overall), the component fires appropriate service. If not, events in queue are processed.

To distinguish between RFID or QR code detection, service assumes QR codes contains `Minus` char.

## Error workarounds

### Typing certain keys leads to infinite loop of event firings

Make sure the component is initiated only once. If you need capturing from several components, it should be used in one place (typically root component) and should be listened via singleton service.

## TODO:

- ignore selected inputs (as a queueing can disrupt keypress handles on special inputs)
- show state of key listening
- watch delay between entering the individual characters instead of monitoring time to enter the entire code

## Library deployment:

Run in bash:

```
npm run package
```

### Code scaffolding

Run `ng generate component component-name --project timcontrol-code-reader` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module --project timcontrol-code-reader`.
> Note: Don't forget to add `--project timcontrol-code-reader` or else it will be added to the default project in your `angular.json` file. 

### Build

Run `ng build timcontrol-code-reader` to build the project. The build artifacts will be stored in the `dist/` directory.

### Publishing

After building your library with `ng build timcontrol-code-reader`, go to the dist folder `cd dist/timcontrol-code-reader` and run `npm publish`.

### Running unit tests

Run `ng test timcontrol-code-reader` to execute the unit tests via [Karma](https://karma-runner.github.io).

### Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

***

This library was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.1.2.
