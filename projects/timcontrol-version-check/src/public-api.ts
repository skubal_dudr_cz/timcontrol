/*
 * Public API Surface of timcontrol-version-check
 */

export * from './lib/timcontrol-version-check.service';
export * from './lib/timcontrol-version-check.module';
