/*jshint esversion: 6 */

/**
 * @author Henrik Peinar (https://blog.nodeswat.com/@hpeinar)
 * @see https://blog.nodeswat.com/automagic-reload-for-clients-after-deploy-with-angular-4-8440c9fdd96c
 */

const git  = require('git-rev-sync');

const path = require('path');
const fs = require('fs');
const util = require('util');

const basePath = process.cwd();

const appName = require(path.join(basePath, 'package.json')).name;
const appVersion = require(path.join(basePath, 'package.json')).version;

const readDir = util.promisify(fs.readdir);
const writeFile = util.promisify(fs.writeFile);
const readFile = util.promisify(fs.readFile);

console.log('\nRunning post-build tasks');

const versionFilePath = path.join(basePath, 'dist', appName, 'version.json');
let versionJson = {
  appVersion: appVersion,
  bundle: null,
  stylesheet: null,
  gitInfo: null,
};

let mainBundleFile = '';
let stylesheetFile = '';

let mainBundleRegexp = /^main-es2015.?([a-z0-9]*)?.js$/;
let stylesheetRegexp = /^styles.?([a-z0-9]*)?.css$/;

readDir(path.join(basePath, 'dist', appName))
  .then(files => {

    mainBundleFile = files.find(f => mainBundleRegexp.test(f));
    if (mainBundleFile) {
      let matchHash = mainBundleFile.match(mainBundleRegexp);

      // if it has a hash in it's name, mark it down
      if (matchHash.length > 1 && !!matchHash[1]) {
        versionJson.bundle = matchHash[1];
      }
    }

    stylesheetFile = files.find(f => stylesheetRegexp.test(f));
    if (stylesheetFile) {
      let matchHash = stylesheetFile.match(stylesheetRegexp);

      // if it has a hash in it's name, mark it down
      if (matchHash.length > 1 && !!matchHash[1]) {
        versionJson.stylesheet = matchHash[1];
      }
    }

    versionJson.gitInfo = {
      commit: git.short(),
      commitLong: git.long(),
      branch: git.branch(),
    };

    versionJson.hash = hashCode(JSON.stringify(versionJson)).toString();
    console.log(versionJson);
    console.log(`Writing version metadata to ${versionFilePath}`);

    return writeFile(versionFilePath, JSON.stringify(versionJson));
  }).then(() => {
    // main bundle file not found, is it development build?
    if (!mainBundleFile || !stylesheetFile) {
      console.log(`${appName}: Version checking is disabled (some files was not found)`);
      return;
    }

    console.log(`Replacing hash in the ${mainBundleFile}`);

    // replace hash placeholder in our main.js file so the code knows it's current hash
    const mainFilepath = path.join(basePath, 'dist', appName, mainBundleFile);
    return readFile(mainFilepath, 'utf8')
      .then(mainFileData => {
        const replacedFile = mainFileData.replace('{{POST_BUILD_ENTERS_HASH_HERE}}', versionJson.hash);
        return writeFile(mainFilepath, replacedFile);
      });
  }).catch(err => {
    console.log('Error with post build:', err);
  });

function hashCode(str) {
  return Array.from(str)
    .reduce((s, c) => Math.imul(31, s) + c.charCodeAt(0) | 0, 0);
}
