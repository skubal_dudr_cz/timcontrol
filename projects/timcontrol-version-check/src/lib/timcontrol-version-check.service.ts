import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

export class GitInfo {
  commit: string;
  commitLong: string;
  branch: string;
}

@Injectable({
  providedIn: 'root',
})
export class TimcontrolVersionCheckService {
  // this will be replaced by actual hash post-build.js
  private currentHash = '{{POST_BUILD_ENTERS_HASH_HERE}}';
  private gitInfo: GitInfo;

  constructor(private http: HttpClient) {}

  /**
   * Checks in every set frequency the version of frontend application
   */
  public initVersionCheck(
    url: string,
    frequency: number = 1000 * 60 * 30, // milliseconds, defaults to 30 minutes
    changeCallback = (hash: string) => {
      location.reload();
    },
    errorCallback = (error: Error) => {
      console.error(error, 'Could not get version');
    }
  ): void {
    setInterval(() => {
      this.checkVersion(url, changeCallback, errorCallback);
    }, frequency);
  }

  /**
   * Will do the call and check if the hash has changed or not
   */
  private checkVersion(url: string, changeCallback, errorCallback): void {
    // timestamp these requests to invalidate caches
    this.http
      .get(url + '?t=' + new Date().getTime())
      //      .first()
      .subscribe(
        (response: any) => {
          const hash = response.hash;
          const hashChanged = this.hasHashChanged(this.currentHash, hash);

          this.gitInfo = response.gitInfo;

          // If new version, do something
          if (hashChanged) {
            changeCallback(hash);
          }
          // store the new hash so we wouldn't trigger versionChange again
          // only necessary in case you did not force refresh
          this.currentHash = hash;
        },
        err => {
          errorCallback(err);
        }
      );
  }

  /**
   * Checks if hash has changed.
   * This file has the JS hash, if it is a different one than in the version.json
   * we are dealing with version change
   */
  private hasHashChanged(currentHash: string, newHash: string): boolean {
    if (!currentHash || currentHash === '{{POST_BUILD_ENTERS_HASH_HERE}}') {
      return false;
    }
    return currentHash !== newHash;
  }

  public getGitInfo(): GitInfo {
    return this.gitInfo;
  }
}
