import { TestBed } from '@angular/core/testing';

import { TimcontrolVersionCheckService } from './timcontrol-version-check.service';

describe('TimcontrolVersionCheckService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TimcontrolVersionCheckService = TestBed.get(TimcontrolVersionCheckService);
    expect(service).toBeTruthy();
  });
});
