import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { TimcontrolVersionCheckService } from './timcontrol-version-check.service';

@NgModule({
  declarations: [],
  imports: [HttpClientModule],
  exports: [],
  providers: [HttpClientModule, TimcontrolVersionCheckService],
})
export class TimcontrolVersionCheckModule {}
